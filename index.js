// const { Frozen } = require('@kano/web-bus');

const DevicesManager = require('@kano/devices-sdk-node');
DevicesManager.setLogger(console);

const LEDS = 9;

async function main() {
  const device = await DevicesManager.searchForDevice('Kano', 10 * 1000)
  if (!device) {
    return;
  }

  // Causes library to connect to device
  console.log('battery', await device.getBatteryStatus())

  // 0: react to sensors using first color provided
  // 1: fixed colors starting right of north sensor
  // 2: blink 3 times, fade south, using first color provided
  const state = 1

  const colors = [
    0x000000,
    0x0000ff,
    0x00ff00,
    0x00ffff,
    0xff0000,
    0xff00ff,
    0xffff00,
    0xffffff,
  ]
  device.setLed(state, colors);

  device.on('proximity', (data) => {
    // device.keepAlive();

    // With the power button oriented towards south, towards the user
    console.log('proximity', {north, east, south, west});
  })
  device.subscribeProximity()


  return 'complete'
}

main().then(console.log).catch(console.error)
